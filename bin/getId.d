#!rdmd

import std.stdio;
import std.json;
import std.file;

void main(string[] args){
    if (args.length < 3) {
        writeln( "0" );
        return;
    }
    try{
        auto j = args[1].readText.parseJSON;
        auto post = j["post"].object;
        auto count = post["attach_count"].integer;
        if (count) {
            foreach ( ref attach; post["Attachments"].array ){
                if ( attach.object["filename"].str == args[2] ) {
                    writeln( attach.object["attachment_id"].integer );
                    return;
                }
            }
        }
    } catch ( Exception e ){}
    writeln( "0" );
}
