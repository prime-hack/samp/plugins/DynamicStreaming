#include "main.h"
#include <psapi.h>
#include <callfunc.hpp>

using namespace std::chrono_literals;

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	// Constructor
	SYSTEM_INFO sysInfo;
	GetSystemInfo( &sysInfo );
	pageSize = sysInfo.dwPageSize;
	hugePageSize = 512 * pageSize;

	Init2.onBefore += std::tuple{ this, &AsiPlugin::initialize };
	Init2.install( 0, 0, false );
}

AsiPlugin::~AsiPlugin() {
	// Destructor
}

void AsiPlugin::initialize() {
	reserved = memRam() / 4;
	streamAvailable = memRam() - reserved;
	g_class.events->onMainLoop += std::tuple{ this, &AsiPlugin::mainloop };
}

void AsiPlugin::mainloop() {
	auto currTick = tick();
	if ( currTick - lastUpdate < kUpdateInterval ) return;
	lastUpdate = currTick;
	auto nonStreamingMem = memUsage() - streamUsed;

	// resize reserved memory
	if ( ( nonStreamingMem < reserved && reserved - nonStreamingMem > hugePageSize * 16 ) || nonStreamingMem + pageSize >= reserved )
		reserved = nonStreamingMem + hugePageSize * 8;

	// resize streaming memory
	if ( streamAvailable + reserved != memRam() ) resizeStreamMem( memRam() - reserved );

	// Garbage collector
	if ( streamAvailable >= k100MB && streamUsed >= streamAvailable * kGCPercentage ) {
		if ( timerGC == 0ms )
			timerGC = tick();
		else if ( tick() - timerGC > kGCInterval ) {
			timerGC = 0ms;
			clearStream();
		}
	}
}

std::chrono::milliseconds AsiPlugin::tick() {
	return std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() );
}

unsigned long long AsiPlugin::memUsage() {
	MEMORYSTATUSEX memory_status;
	ZeroMemory( &memory_status, sizeof( MEMORYSTATUSEX ) );
	memory_status.dwLength = sizeof( MEMORYSTATUSEX );
	if ( GlobalMemoryStatusEx( &memory_status ) ) {
		PROCESS_MEMORY_COUNTERS mem;
		ZeroMemory( &mem, sizeof( PROCESS_MEMORY_COUNTERS ) );
		GetProcessMemoryInfo( GetCurrentProcess(), &mem, sizeof( PROCESS_MEMORY_COUNTERS ) );
		if ( memory_status.ullTotalVirtual - memory_status.ullAvailVirtual < mem.PagefileUsage )
			memory_status.ullAvailVirtual = memory_status.ullTotalVirtual - ( mem.PagefileUsage + hugePageSize );
		return memory_status.ullTotalVirtual - memory_status.ullAvailVirtual;
	}
	return streamAvailable + reserved;
}

unsigned long long AsiPlugin::memRam() {
	MEMORYSTATUSEX memory_status;
	ZeroMemory( &memory_status, sizeof( MEMORYSTATUSEX ) );
	memory_status.dwLength = sizeof( MEMORYSTATUSEX );
	if ( GlobalMemoryStatusEx( &memory_status ) ) {
		auto ram = memory_status.ullTotalVirtual;
		return ram - hugePageSize;
	}
	return k2GB;
}

void AsiPlugin::resizeStreamMem( unsigned int limit ) {
	if ( !limit ) limit = streamAvailable;
	if ( streamUsed >= limit ) clearStream( limit );
	if ( limit != streamAvailable ) streamAvailable = limit;
}

void AsiPlugin::clearStream( unsigned int limit ) {
	if ( !limit ) limit = streamAvailable;
	if ( limit >= k512MB )
		CallFunc::ccall( 0x40E120, limit / 2 );
	else
		CallFunc::ccall( 0x40E120, limit / 4 );
}
