#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>
#include <chrono>
#include <SRHook.hpp>
#include <array>

class AsiPlugin : public SRDescent {
	static constexpr std::chrono::seconds kUpdateInterval{ 1 };

	static constexpr unsigned long long k100MB = 100 * 1024 * 1024;
	static constexpr unsigned long long k512MB = 512 * 1024 * 1024;
	static constexpr unsigned long long k2GB = 2047 * 1024 * 1024;

	static constexpr std::chrono::minutes kGCInterval{ 1 };
	static constexpr auto kGCPercentage = 0.9;

	SRHook::Hook<> Init2{ 0x5B8E78, 5 };

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	void initialize();
	void mainloop();

private:
	unsigned long pageSize = 4096; // 4KB
	unsigned long hugePageSize = 2048 * 1024; // 2MB
	unsigned long long reserved = k512MB; // 512MB
	const unsigned int &streamUsed = *(const unsigned int *)0x8E4CB4;
	unsigned int &streamAvailable = *(unsigned int *)0x8A5A80;
	std::chrono::milliseconds lastUpdate{ 0 };
	std::chrono::milliseconds timerGC{ 0 };

	std::chrono::milliseconds tick();
	unsigned long long memUsage();
	unsigned long long memRam();
	void resizeStreamMem( unsigned int limit = 0 );
	void clearStream( unsigned int limit = 0 );
};

#endif // MAIN_H
